
var ngApp = angular.module("app", ["ngSanitize"]);

ngApp.factory("appService", function($q, $http) {
   var service = {
      trimString: function(string, length) {
         console.log("trim", string, length);
         if (string.length > length) {
            string = string.substring(0, length - 3) + "...";
            console.log("trimmed", string);
         }
         return string;
      },
      trimDataItem: function(dataItem) {
         console.log("trimDataItem", dataItem);
         dataItem.title = service.trimString(dataItem.title, 50);
         if (dataItem.lead) {
            dataItem.lead = service.trimString(dataItem.lead, 120);
         }
      },
      trimData: function(data) {
         for (var i = 0; i < data.length; i++) {
            service.trimDataItem(data[i]);
         }
         console.log("trimData", data);
         return data;
      },
      httpGetPromise: function(url) {
         var deferred = $q.defer();
         $http.get(url).success(function(data) {
            console.info("httpGetPromise response", url, data);
            deferred.resolve(service.trimData(data));
         }).error(function() {
            console.warn("httpGetPromise failed", url);
         });
         return deferred.promise;
      }      
   };
   return service;
});

ngApp.controller("gq", function($scope, appService) {
   console.log("gq controller");
   $scope.title = "GQ";
   $scope.link = "http://gq.co.za";
   appService.httpGetPromise("http://cf.ngena.com:8080/gqservice/gq").then(function(data) {
      $scope.data = data;
   });
});

ngApp.controller("glamour", function($scope, appService) {
   console.log("glamour controller");
   $scope.title = "Glamour";
   $scope.link = "http://glamour.co.za";
   appService.httpGetPromise("http://cf.ngena.com:8080/gqservice/glamour").then(function(data) {
      $scope.data = data;
   });
});


