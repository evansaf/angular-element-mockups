module.exports = function(grunt){
	grunt.loadNpmTasks('grunt-contrib-uglify'); 
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-compass');
	
	grunt.initConfig({
		
		compass: {
			dev: {
				options: {
					config:'config.rb'
				} // options
			} // dev
		}, // compass

		// uglify: {
		// 	my_target: {
		// 		files: {
		// 			'app/js/script.js' : ['js/*.js']
		// 		} // files
		// 	} // my_target
		// },  // uglify

		watch : {
			options: { livereload: true },
			
			files: ['/*'],
						
			sass: {
				options: {
					livereload: false
				},
				files: ['css/sass/*.sass', 'css/*.css'], 
				tasks: ['compass:dev']
			}, // sass
			
			css: {
				files: ['css/*.css']
				//, tasks: []
			},

			js: {
				files: ['js/*.js']
				//, tasks: []
			},
			
			html: {
				files: ['*.html']
			}, // html
			
		} // watch
		
		
	}); // initConfig
	
	grunt.registerTask('default', 'watch');
	
};	 // exports